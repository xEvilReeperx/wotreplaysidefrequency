import struct, json, sys, cPickle, os, operator

# Constants
WOTREPLAY_MAGICNUMBER = 0x11343212
#REPLAY_VERSION = "0, 8, 6, 0"
BATTLE_TYPE = "ctf"
OUTPUT_FILE = "./sidefrequency_summary.txt"

class MagicNumberError(Exception): pass
class BlockCountError(Exception): pass
class InvalidBlock(Exception): pass
class BrokenReplay(Exception): pass


#===============================================================================
# Pretty printing and formatting borrowed from:
# http://ginstrom.com/scribbles/2007/09/04/pretty-printing-a-table-in-python/
#===============================================================================
import locale
locale.setlocale(locale.LC_NUMERIC, "")

def format_num(num):
    """Format a number according to given places.
    Adds commas, etc. Will truncate floats into ints!"""

    try:
        inum = int(num)
        return locale.format("%.*f", (0, inum), True)

    except (ValueError, TypeError):
        return str(num)

def get_max_width(table, index):
    """Get the maximum width of the given column index"""

    return max([len(format_num(row[index])) for row in table])

def pprint_table(out, table):
    """Prints out a table of data, padded for alignment
    @param out: Output stream (file-like object)
    @param table: The table to print. A list of lists.
    Each row must have the same number of columns. """

    col_paddings = []

    for i in range(len(table[0])):
        col_paddings.append(get_max_width(table, i))

    for row in table:
        # left col
        print >> out, row[0].ljust(col_paddings[0] + 1),
        # rest of the cols
        for i in range(1, len(row)):
            col = format_num(row[i]).rjust(col_paddings[i] + 2)
            print >> out, col,
        print >> out
       
#===============================================================================
# End borrowed code ;)
#===============================================================================

class Replay:
    def __init__(self, mstart, mend, battleresult):
        self.MatchStart = mstart
        self.MatchEnd = mend
        self.BattleResult = battleresult
        
        self.PlayerName = self.MatchStart["playerName"]
        
        # get player side
        vehicles = self.MatchStart["vehicles"]
        
        for value in vehicles.itervalues():
            if value["name"] == self.PlayerName:
                self.PlayerSide = int(value["team"])
                break
            
        if self.PlayerSide == None:
            raise BrokenReplay("'" + self.PlayerName + "' not found in replay teams")
    

        
    # version info
    def GetVersionInfo(self):
        try:
            return self.MatchStart["clientVersionFromExe"]
        except KeyError:
            return "<not found>"
        
        
    
    # Return type of battle ("standard", "encounter", "assault")
    def GetBattleType(self):
        return self.MatchStart["gameplayID"]
    
    
    # Return (display) name of map
    def GetMapName(self):
        return self.MatchStart["mapDisplayName"]
    
    
    # Return true if the recording player's side won this battle
    def WasVictory(self):
        #print("winnerTeam = " + str(self.BattleResult["common"]["winnerTeam"]) + ", while playerSide = " + str(self.PlayerSide))
        return int(self.BattleResult["common"]["winnerTeam"]) == self.PlayerSide
        
        
def ParseReplay(filename):
    def HasMagicNumber(handle):
        (magicNumber,) = struct.unpack("I", handle.read(4))
        return magicNumber == WOTREPLAY_MAGICNUMBER

    
    def GetBlockCount(handle):
        # get block count
        (blockCount,) = struct.unpack("I", handle.read(4))
        return blockCount
    
    
    def GetJsonBlock(handle):
        #######################################################
        # get match start block
        #######################################################
        
        # get size in bytes
        (blockSize,) = struct.unpack("I", handle.read(4))
        
        if blockSize == 0:
            raise InvalidBlock("blockSize == 0")
        
        # get json string representing this block's data
        encodedJson = handle.read(blockSize)
        return json.loads(encodedJson)
        
        
    def GetBattleResultBlock(handle):
        (blockSize,) = struct.unpack("I", handle.read(4))
        
        if blockSize == 0:
            raise InvalidBlock("BattleResult blockSize == 0")
        return cPickle.loads(handle.read(blockSize))
    
    try:
        # make sure this is an actual wotreplay
        with open(filename, "rb") as inh:

            if HasMagicNumber(inh) == False:
                raise MagicNumberError
                

        # get block count
            blockCount = GetBlockCount(inh)
            
            if blockCount < 2 or blockCount > 3:
                raise BlockCountError("BlockCount < 2 or > 3")
            
        
            
            #######################################################
            # get blocks
            #######################################################
            MatchStart = GetJsonBlock(inh)
            
            # chunk1: MatchStart
            # chunk2: BattleResult if numblocks = 2, Match End if numblocks = 3
            # chunk3: BattleResult if numblocks = 3
            
            BattleResult = None
            MatchEnd = None
            
            if blockCount == 2:
                BattleResult = GetBattleResultBlock(inh)
            else:
                MatchEnd = GetJsonBlock(inh)
                BattleResult = GetBattleResultBlock(inh)
            
            
            return Replay(MatchStart, MatchEnd, BattleResult)
    
    except Exception, err:
        sys.stderr.write('ERROR: %s\n' % str(err))
        
    except:
        print("Unknown exception")
    
    
# Return list of all .wotreplays in same dir as this script
def EnumerateFiles(directory = ""):
    files = []
    
    if len(directory) == 0:
        files.extend(EnumerateFiles("."))
        directory = "./replays"
        
    if not directory.endswith("/") and not directory.endswith("\\"):
        directory += "/"
        
    try:
        for possibility in os.listdir(directory):
            if possibility.endswith(".wotreplay"):
                files.append(directory + possibility)
    except WindowsError:
        # dir probably doesn't exist
        pass
    
    return files


# Parse all replays.  Keep ones that are version-current
def ParseReplays(files):
    replays = []
    
    replayCount = len(files)
    counter = 0
    
    for replayPath in files:
        counter += 1
        
        print("Parsing replay " + str(counter) + " of " + str(replayCount)+ ": '" + replayPath + "' ...")
        replay = ParseReplay(replayPath)
        
        if replay != None:
            if True: #replay.GetVersionInfo() == REPLAY_VERSION:
                if replay.GetBattleType() == BATTLE_TYPE:
                    replays.append(replay)
                else:
                    print("ERROR: Battle type is not " + BATTLE_TYPE)
            else:
                print("ERROR: Older version.  Skipping.")
        else:
            print("ERROR: Parse failed")
            
    print("Parsed " + str(len(files)) + " replays; " + str(len(replays)) + " are valid.")
    return replays


# Gather side frequency data
def GatherData(replays):
    # Construct a dict out of map names
    Maps = dict()
    
    for replay in replays:
        if not (replay.GetMapName() in Maps):
            Maps[replay.GetMapName()] = {"Team":{"1":{"count":0, "victories":0}, "2":{"count":0, "victories":0}}}

        # increase metrics for this map
        #MapMetrics = Maps[replay.GetMapName()]
        
        Maps[replay.GetMapName()]["Team"][str(replay.PlayerSide)]["count"] += 1
        if replay.WasVictory():
            Maps[replay.GetMapName()]["Team"][str(replay.PlayerSide)]["victories"] += 1

    return Maps


def CountTotalBattles(SingleMap):
    return int(SingleMap["Team"]["1"]["count"]) + int(SingleMap["Team"]["2"]["count"])
                
def CountTotalVictories(SingleMap):
    return int(SingleMap["Team"]["1"]["victories"]) + int(SingleMap["Team"]["2"]["victories"])
             
             
def MapOverallWr(SingleMap):
    totalBattles = CountTotalBattles(SingleMap)
    
    if totalBattles > 0:
        return "{0:.2%}".format(float(CountTotalVictories(SingleMap)) / totalBattles)
    else:
        return "-.--%"
        
        
        
def ComputeMapData(MapData):
    def SideFrequency(which, SingleMap):
        totalBattles = CountTotalBattles(SingleMap)
        
        if totalBattles > 0:
            return "{0:.2%}".format(float(SingleMap["Team"][str(which)]["count"]) / totalBattles)
        else:
            return ""
        
    def SideWr(which, SingleMap):
        if int(SingleMap["Team"][str(which)]["count"]) > 0:
            return "{0:.2%}".format(float(SingleMap["Team"][str(which)]["victories"]) / SingleMap["Team"][str(which)]["count"])
        else:
            return "-.--%"
        
    #Map        Side    Frequency    WRside   WRoverall BattlesOnSide
    #Malinvoka     1           60%   80%      77%      9
    
    tableHeader = ["Map Name", "Side", "Frequency", "WRside", "WRoverall", "Battles"]
    
    table = []
    
    for key, value in MapData.items():
        for i in range(1, 3):
            newRow = [key, i, SideFrequency(i, value), SideWr(i, value), MapOverallWr(value), value["Team"][str(i)]["count"]]
            table.append(newRow)
            
    table.sort(key = lambda row: row[0])
    
    table.insert(0, tableHeader)
    return table

     
def FormatData(MapData, replays):
    try:
        f = open(OUTPUT_FILE, "w")
        
        # write a bit of header info
        try:
            f.write("#####################################################################\n")
            f.write("# World of Tanks Side Frequency Script                              #\n")
            f.write("#####################################################################\n")
            f.write("Replays analyzed: " + str(len(replays)) + "\n")
            f.write("-----------------------------\n")
            f.write("Overall win rate by map: \n\n")
            
            sorted_maps = sorted(MapData.iteritems(), key = operator.itemgetter(0))
            
            #for key, value in MapData.items():
            for key, value in sorted_maps:
                f.write("{:<16}: {:>10}".format(key, MapOverallWr(value)) + "\n")
                    
            f.write("\n\n######################################################################\n")
            f.write("# Specific Details                                                   #\n")
            f.write("######################################################################\n\n")
            
            pprint_table(f, ComputeMapData(MapData))

        finally:
            f.close()
            
    except IOError:
        print("IOError attempting to write to '" + OUTPUT_FILE + "'")
        
if __name__ == "__main__":
    #replay = ParseReplay("batchat_test.wotreplay")
    #print(replay.MatchStart["clientVersionFromXml"])
    #print(replay.MatchStart["playerName"])
    #print("Player team: " + str(replay.GetPlayerSide()))
    #print("Winning team: " + str(replay.BattleResult["common"]["winnerTeam"]))
    #print(json.dumps(replay.MatchStart))
    
    replayFilenames = EnumerateFiles()
    parsedReplays = ParseReplays(replayFilenames)
    FormatData(GatherData(parsedReplays), parsedReplays)
    
    print("Finished!  See " + OUTPUT_FILE + " for detailed info.")